package com.weatherapp.saugatligal.saugatweatherapp.presentation.dagger;

import android.content.Context;


import com.weatherapp.saugatligal.saugatweatherapp.app.concurrent.UiThreadQueue;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherApi;
import com.weatherapp.saugatligal.saugatweatherapp.app.services.WeatherApiService;
import com.weatherapp.saugatligal.saugatweatherapp.app.services.WeatherServiceImpl;
import com.weatherapp.saugatligal.saugatweatherapp.main.WeatherPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    public UiThreadQueue provideUiThreadQueue() {
        return new UiThreadQueue();
    }

    @Provides
    public WeatherPresenter provideWeatherPresenter(UiThreadQueue uiThreadQueue, WeatherApiService service) {
        return new WeatherPresenter(uiThreadQueue, service);
    }

    @Provides
    public WeatherApiService provideWeatherService(WeatherApi api) {
        return new WeatherServiceImpl(api);
    }

//    @Provides
//    public  provideSchoolDetailsPresenter(UiThreadQueue uiThreadQueue, SchoolDirectoryService service) {
//        return new SchoolDetailsPresenter(uiThreadQueue, service);
//    }
}

package com.weatherapp.saugatligal.saugatweatherapp.app.services;

import android.support.annotation.NonNull;


import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel.List;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel.Main;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherApi;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel.WeatherFiveDaysResponse;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherResponse;


import java.util.ArrayList;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class WeatherServiceImpl implements WeatherApiService {

    private WeatherApi api ;
    private String API_KEY = "97c0c14afa1f88f1871b7a20544f8a0a";

    public WeatherServiceImpl(WeatherApi api) {
        this.api = api;
    }

    @Override
    public void getWeather(String lat, String lon , @NonNull Listener callback) {
        api.getWeather("35","139",API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .subscribe(response -> {
                    if (response.isSuccessful()) {
                        WeatherResponse weather = response.body();
                        Main main = setMain(weather);


                        Timber.d("Success response");
                        callback.onData(weather);
                    } else {
                        Timber.d("Request failed");
                        callback.onError();
                    }

                }, error -> {
                    Timber.d("Request failed");
                    Timber.d(error);
                    callback.onError();
                });
    }



    @Override
    public void getWeatherforFiveDays(String lat, String lon ,@NonNull Listener callback) {
        api.getWeatherFivedays("35","139",API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .subscribe(response -> {
                    if (response.isSuccessful()) {
                        WeatherFiveDaysResponse weather = response.body();
                        ArrayList<List> weatherList = new ArrayList() {
                        };
                        for(int i =0 ; i<= weather.getList().size();i++){

                            weatherList.add((List) weather.getList().get(i));
                        }

                        Timber.d("Success response");
                        callback.onData(weatherList.get(0));
                    } else {
                        Timber.d("Request failed");
                        callback.onError();
                    }

                }, error -> {
                    Timber.d("Request failed");
                    Timber.d(error);
                    callback.onError();
                });

    }

    public Main setMain(WeatherResponse response){
        Main main = new Main();
        main.setTemp(response.main.temp);
        main.setHumidity(response.main.humidity);
        main.setPressure(response.main.pressure);
        return main;


    }
}

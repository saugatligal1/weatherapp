package com.weatherapp.saugatligal.saugatweatherapp.app.net;

import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel.Main;

import java.util.List;

public class WeatherResponse {

    public String id;
    public String name;
    public List<Weather> weather ;
    public Main main;

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }
}

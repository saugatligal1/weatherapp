package com.weatherapp.saugatligal.saugatweatherapp.main;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;


import com.weatherapp.saugatligal.saugatweatherapp.R;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel.WeatherFiveDaysResponse;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherResponse;
import com.weatherapp.saugatligal.saugatweatherapp.app.utility.Utility;
import com.weatherapp.saugatligal.saugatweatherapp.presentation.App;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherActivity extends AppCompatActivity  implements Contract.View{

    @Inject
    WeatherPresenter presenter;

    @BindView(R.id.weather_id)
    TextView weatherTv;

    @BindView(R.id.place_id)
    TextView placetv;

    @BindView(R.id.temperature_id)
    TextView temperatureTv;

    @BindView(R.id.pressure_id)
    TextView pressureTv;

    @BindView(R.id.humidity_id)
    TextView humidityTv;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        App.get(this).getAppComponent().inject(this);
        // bind the view using butterknife
        ButterKnife.bind(this);



    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attach(this);

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }



    @Override
    public void showResult(WeatherResponse weather) {
        weatherTv.setText(weather.getName());
        placetv.setText(weather.getId());
        temperatureTv.setText(weather.getMain().getTemp());
        pressureTv.setText(weather.getMain().getPressure());
        humidityTv.setText(weather.getMain().getHumidity());



    }

    @Override
    public void showfivedaysWeather(WeatherFiveDaysResponse weatherFiveDaysResponse) {
        if(weatherFiveDaysResponse!=null){
            Toast.makeText(this,weatherFiveDaysResponse.getCity().toString(),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void showError() {
        Utility.showToast("There is some backend issue. Please try later",this);

    }

    @Override
    protected void onStop() {
        presenter.detach();
        super.onStop();
    }


}

package com.weatherapp.saugatligal.saugatweatherapp.presentation.dagger;

import android.content.Context;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherApi;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.internal.platform.Platform;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    private final Context context;
    private final String baseUrl = "https://api.openweathermap.org";


    public ApiModule(Context context) {
        this.context = context;
    }

    /**
     * for getting httpclient
     * @return
     */
    @Provides
    public OkHttpClient.Builder provideOkHttpClientBuilder() {
        int timeout = 2;
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder()
                .readTimeout(timeout, TimeUnit.MINUTES)
                .writeTimeout(timeout, TimeUnit.MINUTES)
                .connectTimeout(timeout, TimeUnit.MINUTES);
        return okHttpClientBuilder;
    }

    /*
    To get Gson open for retrofit
     */
    @Provides
    @Singleton
    public Gson provideGson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .create();
    }

    @Provides
    @Singleton
    public LoggingInterceptor provideLoggingInterceptor() {
        return new LoggingInterceptor.Builder()
             //   .loggable(BuildConfig.DEBUG)
                .setLevel(Level.BODY)
                .log(Platform.INFO)
                .request("Request")
                .response("Response")
                .addHeader("version", "1.0")
                .build();
    }

    @Provides
    public Retrofit.Builder provideRetrofitBuilder(OkHttpClient.Builder builder,
                                                   Gson gson,
                                                   LoggingInterceptor loggingInterceptor) {
        return new Retrofit.Builder()
                .client(builder
                        .addInterceptor(loggingInterceptor)
                        .build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                ;
    }

    @Singleton
    @Provides
    public Retrofit provideRetrofit(Retrofit.Builder builder) {
        return builder
                .baseUrl(baseUrl)
                .build();
    }

    @Provides
    public WeatherApi provideWeatherApi(Retrofit retrofit) {
        return retrofit.create(WeatherApi.class);
  }

}

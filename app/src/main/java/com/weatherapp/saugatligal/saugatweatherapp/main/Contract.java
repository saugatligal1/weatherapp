package com.weatherapp.saugatligal.saugatweatherapp.main;

import com.weatherapp.saugatligal.saugatweatherapp.app.net.Weather;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel.WeatherFiveDaysResponse;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherResponse;

public class Contract {

    interface View {
        void showProgress();

        void hideProgress();

        void showResult(WeatherResponse weather);

        void showfivedaysWeather(WeatherFiveDaysResponse weatherFiveDaysResponse);

        void showError();

     //   void launchDetails(HighSchool school);
    }

    interface Presenter {
        void attach(View view);

        void detach();


    }
}

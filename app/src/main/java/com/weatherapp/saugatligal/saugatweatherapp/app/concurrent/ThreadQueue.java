package com.weatherapp.saugatligal.saugatweatherapp.app.concurrent;


public class ThreadQueue {
    private boolean isEnabled;
    private ThreadQueue.ThreadRunner threadRunner;

    ThreadQueue(ThreadQueue.ThreadRunner threadRunner) {
        this.threadRunner = threadRunner;
    }

    public void run(Runnable runnable) {
        if (this.isEnabled()) {
            this.threadRunner.run(runnable);
        }

    }

    public void runDelayed(Runnable runnable, long delayMilliseconds) {
        if (this.isEnabled()) {
            this.threadRunner.runDelayed(runnable, delayMilliseconds);
        }

    }

    public boolean isEnabled() {
        return this.isEnabled;
    }

    public ThreadQueue setEnabled(boolean enabled) {
        this.isEnabled = enabled;
        if (!this.isEnabled()) {
            this.clear();
        }

        return this;
    }

    public void clear() {
        this.threadRunner.clear();
    }

    public interface ThreadRunner {
        void run(Runnable var1);

        void runDelayed(Runnable var1, long var2);

        void clear();
    }
}

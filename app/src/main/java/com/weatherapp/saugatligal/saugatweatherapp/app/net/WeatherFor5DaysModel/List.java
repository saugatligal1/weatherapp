package com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel;

import com.weatherapp.saugatligal.saugatweatherapp.app.net.Weather;

public class List {
    public java.util.List<Weather> weather ;
    public Main main;
    public String dt_txt;

    public java.util.List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(java.util.List<Weather> weather) {
        this.weather = weather;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }
}

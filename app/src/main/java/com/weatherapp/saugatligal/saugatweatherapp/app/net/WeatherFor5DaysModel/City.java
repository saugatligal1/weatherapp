package com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel;

public class City {

    private String name;
    private Cordinate cordinate;
    private String country;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Cordinate getCordinate() {
        return cordinate;
    }

    public void setCordinate(Cordinate cordinate) {
        this.cordinate = cordinate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}

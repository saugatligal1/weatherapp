package com.weatherapp.saugatligal.saugatweatherapp.presentation;

import android.app.Application;
import android.content.Context;

import com.weatherapp.saugatligal.saugatweatherapp.presentation.dagger.ApiModule;
import com.weatherapp.saugatligal.saugatweatherapp.presentation.dagger.AppComponent;
import com.weatherapp.saugatligal.saugatweatherapp.presentation.dagger.AppModule;
import com.weatherapp.saugatligal.saugatweatherapp.presentation.dagger.DaggerAppComponent;


public class App extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .apiModule(new ApiModule(getApplicationContext()))
                .appModule(new AppModule(getApplicationContext()))
                .build();
    }

    public static App get(Context context) {
        return  (App) context.getApplicationContext();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}

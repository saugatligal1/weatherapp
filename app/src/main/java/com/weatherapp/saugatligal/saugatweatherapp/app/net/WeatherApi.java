package com.weatherapp.saugatligal.saugatweatherapp.app.net;

import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel.WeatherFiveDaysResponse;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherApi {
    //used for getting list of NYC SCHOOLS
    @GET("/data/2.5/weather?")
    Observable<Response<WeatherResponse>> getWeather(@Query("lat") String one, @Query("lon") String two,
                                                     @Query("appid") String key);

    @GET("/data/2.5/forecast?")
    Observable<Response<WeatherFiveDaysResponse>> getWeatherFivedays(@Query("lat") String one, @Query("lon") String two,
                                                                     @Query("appid") String key);
}


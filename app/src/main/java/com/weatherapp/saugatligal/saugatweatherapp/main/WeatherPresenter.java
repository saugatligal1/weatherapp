package com.weatherapp.saugatligal.saugatweatherapp.main;

import android.widget.Toast;

import com.weatherapp.saugatligal.saugatweatherapp.app.concurrent.UiThreadQueue;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.Weather;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel.WeatherFiveDaysResponse;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherResponse;
import com.weatherapp.saugatligal.saugatweatherapp.app.services.WeatherApiService;

import timber.log.Timber;

public class WeatherPresenter implements Contract.Presenter {

    private Contract.View view;
    private final UiThreadQueue uiThreadQueue;
    private WeatherApiService weatherService;
    private WeatherResponse weather;
    private WeatherFiveDaysResponse weatherFiveDaysResponse;

    public WeatherPresenter(UiThreadQueue uiThreadQueue, WeatherApiService weatherapiService) {
        this.uiThreadQueue = uiThreadQueue;
        this.weatherService = weatherapiService;
    }

    @Override
    public void attach(Contract.View view) {
        this.view = view;
        uiThreadQueue.setEnabled(true);
        uiThreadQueue.run(() -> view.showProgress());


        /**
         * call for getting data current weather details
         */
        weatherService.getWeather("35", "139", new WeatherApiService.Listener() {
            @Override
            public void onData(Object data) {
                weather = (WeatherResponse)data;
                uiThreadQueue.run(() -> {
                    view.hideProgress();
                    view.showResult(weather);
                });
            }

            @Override
            public void onError() {
                Timber.d("onError");

            }
        });




        /**
         * call for getting data 5 days of  weather details
         */

        weatherService.getWeatherforFiveDays("35", "139", new WeatherApiService.Listener() {
            @Override
            public void onData(Object data) {
                weatherFiveDaysResponse = (WeatherFiveDaysResponse) data;
                uiThreadQueue.run(() -> {
                    view.hideProgress();
                    view.showfivedaysWeather(weatherFiveDaysResponse);
                });
            }

            @Override
            public void onError() {

            }
        });



    }

    @Override
    public void detach() {

    }
}

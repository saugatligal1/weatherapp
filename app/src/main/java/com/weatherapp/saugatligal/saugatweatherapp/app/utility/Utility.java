package com.weatherapp.saugatligal.saugatweatherapp.app.utility;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

/**
 * Created by Owner on 3/28/2018.
 */

public  class Utility {

    private static ProgressDialog progressDialog;

    /*
    * Create an progaress dialog instance
     * @param Context contex
     * @Title title of String
     *
    * */
    public static ProgressDialog createProgressDialog(Context context, String title) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(title);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        return progressDialog;
    }

    /*
    * It dimiss progress dialoge if already visible
    *
    * */
    public static void dissmisProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    /**
     * Show a progress dialoge
     */

    public static void showProgress() {
        if (progressDialog != null)
            progressDialog.show();
    }


    /* Toast message*/
    public static void showToast(String message, Context context){
        Toast.makeText(context,message, Toast.LENGTH_LONG).show();
    }

    /**getting location **/
    public void getLocation(){

    }




}
